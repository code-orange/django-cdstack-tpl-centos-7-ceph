import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7.views import (
    get_yum_packages,
    get_yum_sources,
)
from django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7.views import (
    handle as handle_centos_7,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    module_prefix = "django_cdstack_tpl_centos_7_ceph/django_cdstack_tpl_centos_7_ceph"

    if "yum_packages" not in template_opts:
        template_opts["yum_packages"] = get_yum_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "yum_sources" not in template_opts:
        template_opts["yum_sources"] = get_yum_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_centos_7(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
